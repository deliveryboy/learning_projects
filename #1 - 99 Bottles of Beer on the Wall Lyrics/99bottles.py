num_bottles = 99
f = open('output.txt', 'a')

# below writes whole song except the last three lines
for i in range(num_bottles - 1):
    f.write(str(num_bottles) + " bottles of beer on the wall, " + str(num_bottles) + " bottles of beer.\n")
    f.write("Take one down, pass it around, " + str(num_bottles) + " bottles of beer on the wall...\n")
    num_bottles -= 1

# prints last three lines
# one line for the lat bottle of beer (singular) and the two ending lines
f.write(str(num_bottles) + " bottle of beer on the wall, " + str(num_bottles) + " bottle of beer.\n")
f.write("Take one down, pass it around, no more bottles of beer on the wall...\n")
f.write("We've taken them down and passed them around; now we're drunk and passed out!\n")

f.close()
