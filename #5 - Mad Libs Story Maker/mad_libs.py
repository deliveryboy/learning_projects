# Today I went to the zoo.
# I saw a(n) ___(adjective) ___(noun) jumping up and down in its tree.
# He ___(verb, past tense) ___(adverb) through the large tunnel that led to its ___(adjective) ___(noun).
# I got some peanuts and passed them through the cage to a gigantic gray ___(noun) towering above my head.
# Feeding that animal made me hungry. I went to get a ___(adjective) scoop of ice cream.
# It filled my stomach.
# Afterwards I had to ___(verb) ___(adverb) to catch our bus.
# When I got home I ___(verb, past tense) my mom for a ___(adjective) day at the zoo.
word_types = ["adjective", "noun", "verb, past tense", "adverb", "adjective", "noun", "noun", "adjective", "verb", "adverb", "verb, past tense", "adjective"]
# word_types = ["noun", "adjective", "verb", "verb, past tense", "adverb"]
input_words = []

for i in range(12):
	print("Enter a random " + word_types[i])
	input_words.append(input())

# print(input_words)

# f = open("output.txt", "a")

print("""
Today I went to the zoo.\n
I saw a(n) """ + input_words[0] + " "+ input_words[1] + """ jumping up and down in its tree.\n
He """ + input_words[2] + " " + input_words[3] + " through the large tunnel that led to its " + input_words[4] + " " + input_words[5] + """\n
I got some peanuts and passed them through the cage to a gigantic gray """ + input_words[6] + """ towering above my head.\n
Feeding that animal made me hungry. I went to get a """ + input_words[7] + """ scoop of ice cream.\n
It filled my stomach.\n
Afterwards I had to """ + input_words[8] + " " + input_words[9] + """ to catch our bus.\n
When I got home I """ + input_words[10] + """ my mom for a """ + input_words[11] + """ day at the zoo.
	""")