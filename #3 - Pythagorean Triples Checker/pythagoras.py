sides = []

# user input for three triangle sides, in any order
print("Enter the length of the first side")
sides.append(int(input()))
print("Enter the length of the second side")
sides.append(int(input()))
print("Enter the length of third side")
sides.append(int(input()))

# assigns the highest value as a separate variable, side c
c = max(sides)
# removes c from the sides list so only the smaller sides remain
sides.remove(c)

# Pythagorean theorem, prints only if three sides are a Pythagorean triple
if (sides[0] ** 2) + (sides[1] ** 2) == c**2:
    print("Your triangle is a pythagorean triple")
else:
    print("Your triangle is not a pythagorean triple")
