from time import sleep
from random import choice

# ask for the user's question and then void it
print("Enter your question to the Almighty Magic Ball (of Doom):")
user_question = input()
user_question = None

# simulate ball thinking time
print("The Ball is thinking...")
sleep(2)

# get answers from a pre-set text file
answers = []
fd = open('answers.txt', 'r')
for line in fd:
    answers.append(line.strip())

# print the result
print("The Almighty Magic Ball (of Doom) says: " + choice(answers))
