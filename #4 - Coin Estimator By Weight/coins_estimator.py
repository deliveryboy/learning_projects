# weight of coins is stored and processed in grams
coin_mass = {
	"Penny": 2.5,
	"Nickel": 5,
	"Dime": 2.268,
	"Quarter": 5.6,
	"Half dollar": 11.34
}

# how many coins fit inside each wrapper
coin_wrappers = {
	"Penny": 50,
	"Nickel": 40,
	"Dime": 50,
	"Quarter": 40,
	"Half dollar": 20
}

# print menu for user to select the coin type
# in Python 3.7+ dictionaries are always in the order of incertion. So this works properly
for item in list(coin_mass.keys()):
	print(str(list(coin_mass.keys()).index(item) + 1) + ". " + item)

# accept user choice of coin type and translate it to the format of the dictionary keys
print("Choose the type of coin:")
chosen_coin = list(coin_mass.keys())[int(input()) - 1]

# user choice of weight system
print("Choose the weight system:\n1. Grams\n2. Pounds")
weight_system = int(input())

# get the total weight of the coins from the user
# if entered in pounds, convert the weight to grams
print("Enter the total weight of your coins:")
total_weight = float(input())
if weight_system == 2:
	total_weight = total_weight * 453.59237

# calculate the total number of coins
number_of_coins = total_weight // coin_mass.get(chosen_coin)

# calculate the number of *full* wrappers
number_of_wrappers = int(number_of_coins // coin_wrappers.get(chosen_coin))

#calculate the number of coins which were left and could not fill another wrapper
leftover_coins = int(number_of_coins % coin_wrappers.get(chosen_coin))

# result output
print("Wrappers needed:\n" + str(number_of_wrappers))
print("Additional coins left over\n" + str(leftover_coins))
